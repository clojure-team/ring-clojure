Source: ring-clojure
Section: java
Priority: optional
Maintainer: Debian Clojure Maintainers <team+clojure@tracker.debian.org>
Uploaders:
 Apollon Oikonomopoulos <apoikos@debian.org>,
 Jérôme Charaoui <jerome@riseup.net>,
Build-Depends: debhelper-compat (= 13),
               default-jdk-headless,
               javahelper,
               maven-repo-helper,
               libclojure-java (>= 1.11.1-2),
               libring-codec-clojure (>= 1.1.2),
               libcommons-io-java,
               libcommons-fileupload-java,
               libclj-time-clojure (>= 0.15.2) <!nocheck>,
               libcrypto-random-clojure (>= 1.2.1),
               libcrypto-equality-clojure (>= 1.0.0-2),
               libservlet-api-java,
               libjetty9-java,
               libclj-http-clojure,
               leiningen,
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/clojure-team/ring-clojure.git
Vcs-Browser: https://salsa.debian.org/clojure-team/ring-clojure
Homepage: https://github.com/ring-clojure/ring
Rules-Requires-Root: no

Package: libring-core-clojure
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends},
         libclojure-java,
         libring-codec-clojure,
         libcommons-io-java,
         libcommons-fileupload-java,
         libclj-time-clojure,
         libcrypto-random-clojure,
         libcrypto-equality-clojure,
Recommends: ${java:Recommends}
Description: Clojure web applications library
 Ring is a Clojure web applications library inspired by Python's WSGI
 and Ruby's Rack. By abstracting the details of HTTP into a simple,
 unified API, Ring allows web applications to be constructed of modular
 components that can be shared among a variety of applications, web
 servers, and web frameworks.
 .
 This package contains the core Ring library.

#TODO: missing ns-tracker
#Package: libring-devel-clojure
#Architecture: all
#Depends: ${java:Depends}, ${misc:Depends}
#Recommends: ${java:Recommends}
#Description: Clojure web applications library - development
# Ring is a Clojure web applications library inspired by Python's WSGI
# and Ruby's Rack. By abstracting the details of HTTP into a simple,
# unified API, Ring allows web applications to be constructed of modular
# components that can be shared among a variety of applications, web
# servers, and web frameworks.
# .
# This package contains the Ring development and debugging libraries.

Package: libring-servlet-clojure
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends},
         libring-core-clojure,
         libservlet-api-java,
Recommends: ${java:Recommends}
Description: Clojure web applications library - servlet utilities
 Ring is a Clojure web applications library inspired by Python's WSGI
 and Ruby's Rack. By abstracting the details of HTTP into a simple,
 unified API, Ring allows web applications to be constructed of modular
 components that can be shared among a variety of applications, web
 servers, and web frameworks.
 .
 This package contains the Ring servlet utility library.

Package: libring-jetty-adapter-clojure
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends},
         libring-core-clojure,
         libring-servlet-clojure,
         libjetty9-java,
         libclj-http-clojure,
Recommends: ${java:Recommends}
Description: Clojure web applications library - Jetty adapter
 Ring is a Clojure web applications library inspired by Python's WSGI
 and Ruby's Rack. By abstracting the details of HTTP into a simple,
 unified API, Ring allows web applications to be constructed of modular
 components that can be shared among a variety of applications, web
 servers, and web frameworks.
 .
 This package contains the Ring Jetty adapter.
